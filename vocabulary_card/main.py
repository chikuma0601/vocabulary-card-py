import argparse
import os
from test import Test


if __name__ == "__main__":
    print("Vocabulary Card PY")
    print("v1.0, Created by Chikuma")

    # set up color=True if using nt
    if os.name == "nt":
        os.system("color")
        print("setup color for nt!") 

    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Word bank file path")

    args = parser.parse_args()

    print(args.file)

    test = Test(args.file)
    test.build()