#
# test.py
#
#
#
import os
from subprocess import call

import pandas as pd
from pynput import keyboard
from termcolor import colored

class Test:
    def __init__(self, word_bank_path):

        self.answers = []

        self.clean_screen()
        print("Creating a quick test\n")
        print("Reading from: {}".format(word_bank_path))

        self.words = pd.read_csv(word_bank_path)
        if self.words is None:
            print("Path does not exist.")
        else:
            print("Collected {} words...".format(len(self.words.index)))

    def build(self):
        # shuffle the words (rows)
        self.words = self.words.sample(frac=1).reset_index(drop=True)
        
        print("\n\nPress ENTER to start")

        def on_press(key):
            pass

        def on_release(key):
            if key == keyboard.Key.enter:
                return False

        with keyboard.Listener(on_press=on_press,
                               on_release=on_release) as listener:
            listener.join();
        _ = input() # catch the enter pressed
        print("Start!")
        self.clean_screen()
        self.start_test()

    def start_test(self):
        # cache to make it faster (if the dataframe is big af)
        word_count = len(self.words.index)
        for i in range(word_count):
            print("Q({}/{}):\n\n".format(i + 1, word_count))
            print("    {}".format((self.words.iloc[[i]]["meaning"].values[0])))
            print("\n\n    > ", end="")
            self.answers.append(input())
            self.clean_screen()

        # summarize the test
        correct_count = 0
        print("No.\tWord\tAnswer\tMeaning")
        for i in range(word_count):
            word = self.words.iloc[[i]]
            color = "red"
            if word["word"].values[0] == self.answers[i]:
                correct_count += 1
                color = "green"
            print("{}-\t{}\t{}\t{}".format(i,
                  colored(word["word"].values[0], "yellow" if color == "red" else "white"),
                  colored(self.answers[i] if self.answers[i] != "" else "X", color),
                  word["meaning"].values[0]))

        print("==========END OF TEST===========")
        print("Out of {} vocabularies, {} passed, {} failed".format(
              word_count, correct_count, word_count - correct_count))
        print("Correctness = {}%".format(correct_count / word_count * 100))

    def clean_screen(self):
        os.system("clear") if os.name == "posix" else os.system("CLS")
